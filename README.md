# app.drey

Use `app.drey.` as app ID or developer ID prefix.

### Register an App Name

Create a merge request which adds your `app.drey.<AppName>` app ID of choice to this file.

- The GNOME code of conduct applies
- The [app naming guidelines](https://developer.gnome.org/hig/guidelines/app-naming.html) might be loosly enforced
- You don't obtain any legal rights by registering the domain here

### Register a Developer ID

Create a merge request which adds your `app.drey.<Developer>` developer name of choice to this file. This can be a group of people or just a person. You can also use your `app.drey` app ID as a developer ID without additional registration.

- The GNOME code of conduct applies
- You don't obtain any legal rights by registering the domain here

### Verify on Flathub

Add your token, provided by Flathub, to the [org.flathub.VerifiedApps.txt](org.flathub.VerifiedApps.txt) file by creating a merge request.

### Registered Names

#### Provisionally Registered

Registered for one year. Made permanent if available on Flathub or becomes unregistered again.

- [app.drey.Briefcase](https://gitlab.gnome.org/pervoj/Briefcase) (until 2024-05-08)
- app.drey.Lemmings (until 2025-01-01)
- [app.drey.MeetingPoint](https://gitlab.gnome.org/lwildberg/meeting-point) (until 2023-12-26)
- [app.drey.PaperPlane](https://github.com/paper-plane-developers/paper-plane) (until 2024-05-15)
- [app.drey.Pigeon](https://github.com/pervoj/Pigeon) (until 2023-08-02)
- [app.drey.Planner](https://gitlab.gnome.org/World/planner) (until 2023-10-30)
- [app.drey.Refrain](https://github.com/pervoj/Refrain) (until 2023-08-02)
- [app.drey.Receiver](https://gitlab.gnome.org/j400/receiver) (until 2023-08-20)
- [app.drey.Replay](https://github.com/nahuelwexd/Replay) (until 2023-05-06)
- app.drey.Slider (until 2023-09-26)
- [app.drey.Toggle](https://gitlab.com/OroWith2Os/toggle) (until 2024-11-20)
- [app.drey.Vocalis](https://gitlab.gnome.org/World/vocalis) (until 2025-01-09)
- [app.drey.Doggo](https://codeberg.org/SOrg/Doggo) (until 2025-02-18)
- [app.drey.Highscore](https://gitlab.gnome.org/World/highscore) (until 2025-05-16)
- [app.drey.Navigator](https://gitlab.gnome.org/World/navigator) (until 2025-09-27)
- [app.drey.Housemaid](https://gitlab.gnome.org/monster/housemaid) (until 2025-11-05)
- [app.drey.Typewriter](https://gitlab.gnome.org/JanGernert/typewriter) (until 2025-12-10)

#### Permanently Registered Apps IDs

Apps get permanently registered as soon as they are released on Flathub.

- [app.drey.Biblioteca](https://flathub.org/apps/details/app.drey.Biblioteca)
- [app.drey.Blurble](https://flathub.org/apps/details/app.drey.Blurble)
- [app.drey.Damask](https://flathub.org/apps/details/app.drey.Damask)
- [app.drey.Dialect](https://flathub.org/apps/details/app.drey.Dialect)
- [app.drey.EarTag](https://flathub.org/apps/details/app.drey.EarTag)
- [app.drey.Elastic](https://flathub.org/apps/details/app.drey.Elastic)
- [app.drey.KeyRack](https://flathub.org/apps/details/app.drey.KeyRack)
- [app.drey.MultiplicationPuzzle](https://flathub.org/apps/app.drey.MultiplicationPuzzle)
- [app.drey.Warp](https://flathub.org/apps/details/app.drey.Warp)

#### Permanently Registered Developer IDs

- [app.drey.alicem](https://gitlab.gnome.org/alicem)
- [app.drey.sophieherold](https://gitlab.gnome.org/sophie-h)

### Misc

- [What is a drey?](https://en.wikipedia.org/wiki/Drey)
